FROM python:3-alpine
LABEL org.opencontainers.image.authors="Steven Pritchard <steve@sicura.us>"

RUN pip install \
  --disable-pip-version-check \
  --no-input \
  --no-cache-dir \
  yamllint'>=1,<2'

CMD ["/usr/local/bin/yamllint", "--help"]
