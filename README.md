# `yamllint` container

This repo builds a container with
[`yamllint`](https://github.com/adrienverge/yamllint) on top of the
[Alpine-based Python container](https://github.com/docker-library/python)
using GitLab CI.

## Building the container

Containers are built using
[`kaniko`](https://github.com/GoogleContainerTools/kaniko) whenever a tag is
added to the repo.  The result of the build job will be pushed to
`$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG` and `$CI_REGISTRY_IMAGE:latest`.

In addition, tagging and building will be triggered automatically as changes
are merged.

## Testing

All CI pipelines perform tests using `yamllint` (to check [`.gitlab-ci.yml`](.gitlab-ci.yml)) and [`hadolint`](https://github.com/hadolint/hadolint) (to lint [`Dockerfile`](Dockerfile)).
